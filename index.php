<?php
error_reporting(E_ALL & ~E_NOTICE);

require_once "function.php";
#-----------------------------------------------------------
# 基本設定
#-----------------------------------------------------------

# 管理パスワード(1234)
$admin_id = "yamada";
$md5_pw = "81dc9bdb52d04dc20036dbd8313ed055";

#データベース情報
$testuser ="root";
$testpass ="";
$host ="localhost";
$datebase ="board";

# テンプレートディレクトリ
$tmpl_dir = "./tmpl";


#-----------------------------------------------------------
# ページの表示
#-----------------------------------------------------------
parse_form();
user_auth();

if($in["mode"] == "login" || $in["mode"] == ""){ login(); }//ログイン画面表示
else if($in["mode"] == "logout"){ logout(); }//ログアウト画面表示
else if($in["mode"] == "registration"){
	registration(); 
	try {
		$db = new PDO("mysql:host={$host}; dbname={$datebase}; charset=utf8", $testuser, $testpass);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if($in["mode"] == "post"){
			if($in["state"] == "registration") { user_insert(); }
		}
	}catch (PDOException $e) {
		die ("PDO Error:" . $e->getMessage());
	}
	}//登録画面表示
else if(isset($_SESSION["input_id"]) && isset($_SESSION["input_pw"]) || isset($_SESSION["user_name"]) && isset($_SESSION["password"])){
	try {
		$db = new PDO("mysql:host={$host}; dbname={$datebase}; charset=utf8", $testuser, $testpass);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if($in["mode"] == "post"){
			if($in["state"] == "insert") { post_insert(); }
			else if($in["state"] == "update") { post_update(); }
			else if($in["state"] == "delete") { post_delete(); }
			post_search();
		}
	}catch (PDOException $e) {
		die ("PDO Error:" . $e->getMessage());
	}
}
else{
	error("不正な処理です");
}

