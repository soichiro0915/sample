<?php

#-----------------------------------------------------------
# 認証機能
#-----------------------------------------------------------
function user_auth(){
	global $in;
	global $admin_id;
	global $md5_pw;
	
	session_start();
	$input_id = $in["input_id"];
	$input_pw = $in["input_pw"];
	if($input_id == $admin_id && md5($input_pw) == $md5_pw){
		$_SESSION["input_id"] = $input_id;
		$_SESSION["input_pw"] = $input_pw;
		$in["mode"] = "post";
	}
}

#-----------------------------------------------------------
# フォーム受け取り
#-----------------------------------------------------------
function parse_form(){
	global $in;

	$param = array();
	if (isset($_GET) && is_array($_GET)) { $param += $_GET; }
	if (isset($_POST) && is_array($_POST)) { $param += $_POST; }
	
	foreach($param as $key => $val) {
		# 2次元配列から値を取り出す
		if(is_array($val)){
			$val = array_shift($val);
		}
		
		# 文字コードの処理
		$enc = mb_detect_encoding($val);
		$val = mb_convert_encoding($val,"UTF-8",$enc);
		
		# 特殊文字の処理
		$val = htmlentities($val,ENT_QUOTES, "UTF-8");

		$in[$key] = $val;
	}
	return $in;
}

#-----------------------------------------------------------
# ログイン画面
#-----------------------------------------------------------
function login(){
	# テンプレート読み込み
	$tmpl = page_read("login");
	echo $tmpl;
	exit;
}

#-----------------------------------------------------------
# ログアウト画面
#-----------------------------------------------------------
function logout(){
	session_destroy();
	
	# テンプレート読み込み
	$tmpl = page_read("logout");
	echo $tmpl;
	exit;
}


#-----------------------------------------------------------
# アカウント登録画面
#-----------------------------------------------------------
function registration(){
	session_destroy();
	
	# テンプレート読み込み
	$tmpl = page_read("registration");
	echo $tmpl;
	exit;
}

#-----------------------------------------------------------
# アカウント登録
#-----------------------------------------------------------
function user_insert(){
	global $in;
	global $db;
	global $tmpl_dir;

	#エラーチェック
	$error_notes="";
	if($in["user_name"] == ""){
		$error_notes.="・名前が未入力です。<br>";
	}
	if($in["password"] == ""){
		$error_notes.="・パスワードが未入力です。<br>";
	}
	
	#エラーが存在する場合
	if($error_notes != "") {
		error($error_notes);
	}

	# プリペアードステートメントを準備
	$stmt = $db->prepare('INSERT INTO user (user_name, password) VALUES (:user_name, :password)');

	# 変数を束縛する
	$stmt->bindParam(':user_name', $user_name);
	$stmt->bindParam(':password', $password);

	# 変数に値を設定し、SQLを実行
	$user_name = $in["user_name"];
	$password = $in["password"];
	$stmt->execute();
}

#-----------------------------------------------------------
# 商品登録
#-----------------------------------------------------------
function post_insert(){
	global $in;
	global $db;
	global $tmpl_dir;

	#エラーチェック
	$error_notes="";
	if($in["post_name"] == ""){
		$error_notes.="・題名が未入力です。<br>";
	}
	if($in["comment"] == ""){
		$error_notes.="・コメントが未入力です。<br>";
	}
	
	#エラーが存在する場合
	if($error_notes != "") {
		error($error_notes);
	}

	# プリペアードステートメントを準備
	$stmt = $db->prepare('INSERT INTO post (post_name, comment, post_flag) VALUES (:post_name, :comment, 1)');

	# 変数を束縛する
	$stmt->bindParam(':post_name', $post_name);
	$stmt->bindParam(':comment', $comment);

	# 変数に値を設定し、SQLを実行
	$post_name = $in["post_name"];
	$comment = $in["comment"];
	$stmt->execute();
}

#-----------------------------------------------------------
# 商品編集
#-----------------------------------------------------------
function post_update(){
	global $in;
	global $db;
	global $tmpl_dir;

	#エラーチェック
	$error_notes="";
	if($in["post_id"] == ""){
		$error_notes.="・編集する商品を選択してください。<br>";
	}
	if($in["post_name"] == ""){
		$error_notes.="・題名が未入力です。<br>";
	}
	if($in["comment"] == ""){
		$error_notes.="・コメントが未入力です。<br>";
	}
	
	#エラーが存在する場合
	if($error_notes != "") {
		error($error_notes);
	}

	# プリペアードステートメントを準備
	$stmt = $db->prepare('UPDATE post SET post_name = :post_name, comment = :comment where post_id = :post_id');

	# 変数を束縛する
	$stmt->bindParam(':post_id', $post_id);
	$stmt->bindParam(':post_name', $post_name);
	$stmt->bindParam(':comment', $comment);

	# 変数に値を設定し、SQLを実行
	$post_id = $in["post_id"];
	$post_name = $in["post_name"];
	$comment = $in["comment"];
	$stmt->execute();
}

#-----------------------------------------------------------
# 商品削除
#-----------------------------------------------------------
function post_delete(){
	global $in;
	global $db;
	global $tmpl_dir;

	#エラーチェック
	$error_notes="";
	if($in["post_id"] == ""){
		$error_notes.="・削除する投稿を選択してください。<br>";
	}
	
	#エラーが存在する場合
	if($error_notes != "") {
		error($error_notes);
	}

	# プリペアードステートメントを準備
	$stmt = $db->prepare('UPDATE post SET post_flag = 0 WHERE post_id = :post_id');

	# 変数を束縛する
	$stmt->bindParam(':post_id', $post_id);

	# 変数に値を設定し、SQLを実行
	$post_id = $in["post_id"];
	$stmt->execute();
}

#-----------------------------------------------------------
# 商品一覧
#-----------------------------------------------------------
function post_search(){
	global $in;
	global $db;
	global $tmpl_dir;

	# 自身のパス
	$script_name=$_SERVER['SCRIPT_NAME'];

	# SQLを作成
	$query = "SELECT * FROM post WHERE post_flag = 1";
	
	# プリペアードステートメントを準備
	$stmt = $db->prepare($query);
	$stmt->execute();

	$post_data = "";	
	while($row = $stmt->fetch()){
		$post_id = $row["post_id"];
		$post_data .= "<tr>";
		$post_data .= "<td class=\"form-left\">$post_id</td>";
		$post_data .= "<td class=\"form-left\">$row[post_name]</td>";
		$post_data .= "<td class=\"form-left\">$row[comment]</td>";
		$post_data .= "<td><a href=\"$script_name?mode=post&post_id=$post_id\">編集</a></td>";
		$post_data .= "<td><a href=\"$script_name?mode=post&state=delete&post_id=$post_id\">削除</a></td>";
		$post_data .= "</tr>\n";
	}

	if($in["post_id"] != ""){
		# 選択した商品IDに対応する情報を取得
		$stmt = $db->prepare('SELECT * FROM post WHERE post_id = :post_id');
		$stmt->bindParam(':post_id', $post_id);
		$post_id = $in["post_id"];
		$stmt->execute();
		$row = $stmt->fetch();
		$post_name = $row["post_name"];
		$comment = $row["comment"];
		
		# 掲示板テンプレート読み込み
		$tmpl = page_read("post_edit");
		# 文字変換
		$tmpl = str_replace("!post_id!",$in["post_id"],$tmpl);
		$tmpl = str_replace("!post_name!",$post_name,$tmpl);
		$tmpl = str_replace("!comment!",$comment,$tmpl);
		$tmpl = str_replace("!post_data!",$post_data,$tmpl);
	}
	else{
		# 掲示板テンプレート読み込み
		$tmpl = page_read("post");
		# 文字変換
		$tmpl = str_replace("!post_data!",$post_data,$tmpl);
	}
	echo $tmpl;
	exit;
}

#-----------------------------------------------------------
# エラー画面
#-----------------------------------------------------------
function error($errmes){
	global $tmpl_dir;
	$msg = $errmes;

	# エラーテンプレート読み込み
	$tmpl = page_read("error");

	# 文字置き換え
	$tmpl = str_replace("!message!","$msg",$tmpl);
	echo $tmpl;
	exit;
}

#-----------------------------------------------------------
# ページ読み取り
#-----------------------------------------------------------
function page_read($page){
	global $tmpl_dir;
	
	# テンプレート読み込み
	$conf = fopen( "$tmpl_dir/{$page}.tmpl", "r") or die;
	$size = filesize("$tmpl_dir/{$page}.tmpl");
	$tmpl = fread($conf, $size);
	fclose($conf);
	
	return $tmpl;
}

