<meta charset="UTF-8">
<?php

try{
//データベースに接続
$pdo = new PDO('mysql:host=localhost; dbname=board; charset=utf8' , 'root' , '');

//MySQLのエラー表示
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = <<<_SQL_
CREATE TABLE user(
	user_id	INT PRIMARY KEY AUTO_INCREMENT,
	user_name	VARCHAR(100),
	password VARCHAR(20)

);
_SQL_;

$stmt=$pdo->prepare($sql);
$stmt->execute();
echo "userテーブルを作成しました。";

}catch (PDOException $e){
  die('エラーが起きました。：'.$e->getMessage());
}
$pdo = null;
?>
